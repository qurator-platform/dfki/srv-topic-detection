import json
import os
import re

import pickle
from json import JSONDecodeError

import gensim
from gensim import corpora
from cleantext import clean

from topic_detection.corpus import LANGUAGES
from topic_detection.utils import prepare_text_for_lda

"""

https://digital.staatsbibliothek-berlin.de/

"""

dump_dir_root = '/Volumes/data/repo/data/stabi'
docs = []
all_cats = set()
cat_freq = {}


category_mapping = {
    'Historische Drucke':'materialart',
    'Rechtswissenschaft':'fach',
    'Krieg 1914-1918':'fach',
    'Ostasiatica':'fach',
    'Geschichte / Ethnographie / Geographie':'fach',
    'VD18 digital':'materialart',
    'Einblattdrucke':'materialart',
    'Theologie':'fach',
    'Kinder- und Jugendbücher':'materialart',
    'Porträts, Bildmaterialien':'materialart',
    'Sprachen / Literaturen':'fach',
    'Aberglaube / Mystische Philosophie':'fach',
    'Allgemeines / Wissenschaftskunde / Literaturgeschichte':'fach',
    'Musik':'fach',
    'Politik / Staat / Gesellschaft / Wirtschaft':'fach',
    'Kunst':'fach',
    'Landwirtschaft / Forstwirtschaft':'fach',
    'Karten':'materialart',
    'Medizin':'fach',
    'Militärwesen':'fach',
    'Musikdrucke':'materialart',
    'Musiknoten':'materialart',
    'Handschriften':'materialart',
    'Nachlässe und Autographe':'materialart',
    'Architektur / Technik':'fach',
    'Naturwissenschaften / Mathematik':'fach',
    'Sinica':'fach',
    'Philosophie / Psychologie':'fach',
    'Pädagogik':'fach',
    'Zeitungen':'materialart',
    'Orientalische Handschriften':'materialart',
    'Mandschurica':'fach',
    'Slavica':'fach',
    'Tibetica':'fach',
    'Musikhandschriften':'materialart',
    'Afrikanische Handschriften':'materialart',
    'Christlich-orientalische Handschriften':'materialart',
    'Südostasiatische Handschriften':'materialart'
}

"""
with open('models/stabi_docs.pickle', 'rb') as f:
    docs = pickle.load(f)
"""

clean_text = True
docs_limit = 0

for dn in os.listdir(dump_dir_root):
    dump_dir = os.path.join(dump_dir_root, dn)

    if os.path.isdir(dump_dir):  #  and int(dn) < 100

        for fn in os.listdir(dump_dir):
            if 0 < docs_limit < len(docs):
                break

            if fn.endswith('.json'):
                with open(os.path.join(dump_dir, fn)) as fp:
                    try:
                        doc = json.load(fp)
                        meta = {}

                        for item in doc['metadata']:
                            # if item['label'] not in meta:
                            #     meta[item['label']] = []
                            #
                            # meta[item['label']].append(item['value'])
                            meta[item['label']] = item['value']

                        try:
                            # meta['Sprache'] == 'ger' and
                            #if int(meta['Erscheinungsjahr']) > 1900:
                            # and 'Erscheinungsjahr' in meta and int(meta['Erscheinungsjahr']) > 1900
                            if 'Sprache' in meta and meta['Sprache'] == 'ger':
                                # print(fn)

                                if isinstance(meta['Kategorie'], list):
                                    cats = []
                                    for k in meta['Kategorie']:
                                        cats.append(k['@value'])
                                else:
                                    cats = [meta['Kategorie']]

                                topics = []
                                doc_types = []

                                for c in cats:
                                    if c in category_mapping:
                                        if category_mapping[c] == 'fach':
                                            topics.append(c)

                                            if c in cat_freq:
                                                cat_freq[c] += 1
                                            else:
                                                cat_freq[c] = 1

                                        elif category_mapping[c] == 'materialart':
                                            doc_types.append(c)
                                        else:
                                            print('Invalid cat: %s' % c)

                                all_cats.update(cats)

                                with open(os.path.join(dump_dir, fn.replace('manifest.json', 'plaintext.txt'))) as txt_fp:
                                    # doc['text'] = txt_fp.read()
                                    # doc.update(meta)
                                    text = txt_fp.read()

                                    docs.append({
                                        'title': meta['Titel'],
                                        # 'author': meta['Autor'],
                                        #'pub_year': meta['Erscheinungsjahr'],
                                        'categories': cats,
                                        'doc_types': doc_types,
                                        'topics': topics,

                                        'text': clean(text, lang='de', fix_unicode=False) if clean_text else text,
                                    })
                        except KeyError as e:
                            print(f'Something is wrong with {dn}/{fn}: {e}')
                    except JSONDecodeError as e:
                        print(f'Cannot read JSON from  {dn}/{fn}')



[d['topics'] for d in docs]
[d['topics'] for d in docs]
set(t for d in docs for t in d['topics'])

"""

{'Rechtswissenschaft': 746, 
'Krieg 1914-1918': 1878, 
'Allgemeines / Wissenschaftskunde / Literaturgeschichte': 12, 
'Politik / Staat / Gesellschaft / Wirtschaft': 2, 
'Ostasiatica': 957,
 'Geschichte / Ethnographie / Geographie': 456, 
 'Kunst': 17, 'Architektur / Technik': 7,
  'Aberglaube / Mystische Philosophie': 65, 'Theologie': 274,
   'Sprachen / Literaturen': 10, 
   'Musik': 25, 'Sinica': 3, 'Mandschurica': 1}

"""

#


num_topics = len(cat_freq)  #  14
terms_per_topic = 10

selected_cats = set([cat for cat,freq in cat_freq.items() if freq > 1000])
num_topics = len(selected_cats)

cat_freq_out = {c: 100 for c in cat_freq.keys()}
cleaned_docs = []
for doc in docs:
    if len(doc['topics']) > 0:
        t = doc['topics'][0]

        if cat_freq_out[t] == 0:
            continue
        else:
            cat_freq_out[t] -= 1

            cleaned_docs.append(prepare_text_for_lda(LANGUAGES['de'], doc['text'], cut_long_text=True, max_text_length=100000))



# cleaned_docs = [prepare_text_for_lda(LANGUAGES['de'], doc['text'], cut_long_text=True, max_text_length=100000) for doc in docs if len(set(doc['topics']) & selected_cats) > 0]

dictionary = corpora.Dictionary(cleaned_docs)

len(dictionary)

dictionary.filter_extremes()
dictionary.filter_n_most_frequent(100)


corpus = [dictionary.doc2bow(doc) for doc in cleaned_docs]

lda_model = gensim.models.LdaMulticore(
            corpus,
            num_topics=num_topics,
            id2word=dictionary,
            workers=2
        )

for topic_id in range(lda_model.num_topics):
    topic_terms = lda_model.show_topic(topic_id, terms_per_topic)
    print(f'#{topic_id}: {topic_terms}\n')

"""

#0: [('wurde', 0.003656746), ('georg', 0.0031587663), ('schon', 0.002947635), ('leben', 0.00244963), ('heimat', 0.0022290496), ('immer', 0.0021111278), ('worden', 0.0017409315), ('liebe', 0.0017320608), ('pagoda', 0.0016534557), ('deutschen', 0.0015721319)]
#1: [('wurde', 0.003012285), ('schon', 0.002838107), ('recht', 0.002102357), ('leben', 0.0019324537), ('immer', 0.0018496478), ('georg', 0.0018074417), ('heimat', 0.001613945), ('gottes', 0.0015292505), ('pagoda', 0.0014880328), ('liebe', 0.0014074054)]
#2: [('wurde', 0.0033742923), ('georg', 0.0032808455), ('schon', 0.0025622111), ('immer', 0.0023243253), ('heimat', 0.0021980386), ('wurden', 0.0020441783), ('recht', 0.0019270481), ('liebe', 0.0017798956), ('worden', 0.0017254213), ('gottes', 0.001640421)]
#3: [('schon', 0.0034837758), ('wurde', 0.0033150734), ('immer', 0.0020057424), ('recht', 0.0019900925), ('heimat', 0.0019653675), ('deutschen', 0.0019007556), ('georg', 0.001635954), ('worden', 0.0016309555), ('liebe', 0.0016259657), ('wurden', 0.0015122757)]
#4: [('wurde', 0.0032769581), ('schon', 0.0025125179), ('leben', 0.0019369753), ('worden', 0.0018512794), ('liebe', 0.0018284108), ('heimat', 0.0016195128), ('immer', 0.0014761746), ('recht', 0.0014057868), ('heinrich', 0.0013367106), ('lassen', 0.0012613845)]
#5: [('wurde', 0.0032953536), ('leben', 0.002383825), ('schon', 0.0022324917), ('immer', 0.0021783921), ('liebe', 0.0019585204), ('recht', 0.001773199), ('krieg', 0.001569004), ('deutschen', 0.0014907017), ('deutsche', 0.0014631905), ('heimat', 0.0014567792)]
#6: [('wurde', 0.0036443726), ('schon', 0.0031687263), ('leben', 0.0027323568), ('georg', 0.0022398415), ('liebe', 0.0021493575), ('heimat', 0.0020859607), ('worden', 0.0020174559), ('gottes', 0.0016021281), ('immer', 0.0015759072), ('deutschen', 0.0015168252)]
#7: [('wurde', 0.0037085228), ('schon', 0.0025056617), ('immer', 0.00211769), ('liebe', 0.001956401), ('heimat', 0.0017614453), ('leben', 0.0017565119), ('recht', 0.0016082039), ('lassen', 0.0015091312), ('georg', 0.0014814528), ('krieg', 0.0014680774)]
#8: [('wurde', 0.0036451686), ('leben', 0.0024663515), ('deutschen', 0.0021885983), ('schon', 0.0021058056), ('pagoda', 0.0016418577), ('recht', 0.0016279449), ('jahre', 0.0015527632), ('worden', 0.0014559472), ('heimat', 0.0014438183), ('wurden', 0.0012845771)]
#9: [('wurde', 0.0034675158), ('schon', 0.0025272097), ('leben', 0.002322441), ('deutschen', 0.0021401679), ('immer', 0.0020691166), ('liebe', 0.0018001102), ('recht', 0.0017652136), ('wurden', 0.0016013726), ('georg', 0.0015731204), ('gottes', 0.0015489057)]
#10: [('wurde', 0.0037846994), ('leben', 0.0028909154), ('schon', 0.002315376), ('deutschen', 0.00201202), ('heimat', 0.0019393183), ('gottes', 0.0017431412), ('liebe', 0.0016340197), ('vater', 0.0015619213), ('recht', 0.0015159927), ('wurden', 0.0014747159)]
#11: [('wurde', 0.003404455), ('schon', 0.0027327181), ('liebe', 0.0022711053), ('immer', 0.0021029294), ('worden', 0.0020733948), ('pagoda', 0.0018829602), ('heimat', 0.0018774479), ('georg', 0.0018331319), ('leben', 0.0017110592), ('wurden', 0.0017081044)]
#12: [('wurde', 0.0046512657), ('schon', 0.0036877687), ('georg', 0.002210799), ('heimat', 0.0020251963), ('recht', 0.001930493), ('immer', 0.0018800029), ('leben', 0.001844833), ('deutschen', 0.0016850376), ('wurden', 0.0015532506), ('worden', 0.001519321)]
#13: [('wurde', 0.0036639695), ('leben', 0.0025024095), ('heimat', 0.0024351466), ('liebe', 0.0021835477), ('schon', 0.0018159305), ('immer', 0.0016790173), ('unserer', 0.001668562), ('recht', 0.0016666098), ('worden', 0.001615031), ('deutschen', 0.0015755413)]


"""

# Pickle it!
with open('models/stabi_docs.pickle', 'wb') as f:
    pickle.dump(docs, f)

with open('models/stabi_cleaned_docs.pickle', 'wb') as f:
    pickle.dump(cleaned_docs, f)

