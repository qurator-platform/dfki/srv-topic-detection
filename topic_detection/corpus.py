import logging
import os
import pickle

import gensim
import nltk
import numpy as np
import spacy
from gensim import corpora

from topic_detection import Topic
from topic_detection.language import Language
from topic_detection.settings import MODEL_DIR, SPACY_MODEL, NLTK_STOP_WORDS, SPACY_MODEL_DE, \
    NLTK_STOP_WORDS_DE
from topic_detection.utils import prepare_text_for_lda
from topic_detection.wiki import WikiTopics

logger = logging.getLogger(__name__)


LANGUAGES = {
    # TODO lazy load?
    'en': Language(
        nlp=spacy.load(SPACY_MODEL, disable=['parser', 'tagger', 'ner']),
        stop_words=set(
            nltk.corpus.stopwords.words(NLTK_STOP_WORDS) + [
                'would', 'first', 'second', 'third', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight'
                ]
        ),
    ),
    'de': Language(
        nlp=spacy.load(SPACY_MODEL_DE, disable=['parser', 'tagger', 'ner']),
        stop_words = set(nltk.corpus.stopwords.words(NLTK_STOP_WORDS_DE)),
    ),
}


class Corpus(object):
    terms_per_topic = 5
    topics = None

    def __init__(self, name: str, language: Language, num_topics: int, dictionary=None, lda_model=None, model_dir=None):
        self.name = name
        self.language = language
        self.num_topics = num_topics
        self.dictionary = dictionary
        self.lda_model = lda_model
        self.model_dir = model_dir

    def to_dict(self):
        return {
            'name': self.name,
            'language_model': self.language.nlp.meta['name'],
            'num_topics': self.num_topics,
            'topics': {k: t.to_dict() for k, t in self.topics.items()},
        }

    def train(self, docs, wiki_topics: WikiTopics, filter_extreme=False, filter_n_most_frequent=0,
              lda_passes: int = 10):
        """

        :param filter_n_most_frequent:
        :param filter_extreme:
        :param wiki_topics:
        :param docs: List[str] unprocessed text
        :return:
        """

        cleaned_docs = []

        for doc in docs:
            cleaned_docs.append(prepare_text_for_lda(self.language, doc))

        self.dictionary = corpora.Dictionary(cleaned_docs)

        if filter_extreme:
            self.dictionary.filter_extremes()

        if filter_n_most_frequent > 0:
            self.dictionary.filter_n_most_frequent(filter_n_most_frequent)

        # TOOD
        # corpus.dictionary.filter_extremes(no_below=1, no_above=0.5, keep_n=1)

        corpus = [self.dictionary.doc2bow(text) for text in cleaned_docs]

        self.lda_model = gensim.models.LdaMulticore(
            corpus,
            num_topics=self.num_topics,
            id2word=self.dictionary,
            passes=lda_passes,
            workers=2
        )

        self.topics = self.get_topic_labels_from_lda(wiki_topics)

    def get_topic_labels_from_lda(self, wiki_topics):
        topics = {}

        for topic_id in range(self.lda_model.num_topics):
            topic_terms = self.lda_model.show_topic(topic_id, self.terms_per_topic)

            term_vectors = [wiki_topics.get_word_vector(w) for w, _ in topic_terms if wiki_topics.get_word(w)]
            term_weights = [weight for w, weight in topic_terms if wiki_topics.get_word(w)]

            topic_vector = np.average(term_vectors, 0, weights=term_weights)

            wiki_candidates = list(wiki_topics.most_similar_by_entity_vector(topic_vector, self.terms_per_topic))

            topics[topic_id] = Topic(
                topic_id,
                topic_terms,
                term_vectors,
                term_weights,
                topic_vector,
                wiki_candidates,
            )

        return topics

    def get_model_dir(self):
        if self.model_dir is None:
            self.model_dir = os.path.join(MODEL_DIR, self.name)

            logger.debug(f'Model dir: {self.model_dir}')

            if not os.path.exists(self.model_dir):
                logger.debug('Creating new model dir')
                os.makedirs(self.model_dir)

        return self.model_dir

    def get_lda_model_path(self):
        return os.path.join(self.get_model_dir(), 'lda')

    def get_dictionary_path(self):
        return os.path.join(self.get_model_dir(), 'dictionary')

    def get_topics_path(self):
        return os.path.join(self.get_model_dir(), 'topics.pkl')

    def save(self, model_dir=None, override=False):
        if model_dir is not None:
            self.model_dir = model_dir

        if not override and os.path.exists(self.get_model_dir()):
            logger.warning(f'Model directory exists already: {self.model_dir}')
            return

        self.lda_model.save(self.get_lda_model_path())

        self.dictionary.save_as_text(self.get_dictionary_path())

        with open(self.get_topics_path(), 'wb') as f:
            pickle.dump(self.topics, f)

        logger.debug('Corpus saved')

        # self.dictionary.save(os.path.join(MODEL_DIR, self.name, 'lda'))

    def load(self):
        logger.debug('Loading corpus...')

        self.dictionary = corpora.Dictionary.load_from_text(self.get_dictionary_path())
        self.lda_model = gensim.models.LdaMulticore.load(self.get_lda_model_path())

        with open(self.get_topics_path(), 'rb') as f:
            self.topics = pickle.load(f)

    @staticmethod
    def load_from_disk(name, model_dir, language_code, num_topics):
        corpus = Corpus(name, LANGUAGES[language_code], num_topics)
        corpus.model_dir = model_dir
        corpus.load()

        return corpus
