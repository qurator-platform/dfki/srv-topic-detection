"""


Big Knowledge Graph Embeddings (113GB)

https://torchbiggraph.readthedocs.io/en/latest/pretrained_embeddings.html

Format:
- first line:  entity count, the relation type count and the dimension (e.g. 78404883        4151    200)
- other lines: <id> <tsv tensor>
    - e.g.:
        <http://schema.org/Dataset>     0.0041  -0.0306 0.5658  0.1964  0.0869 ...
        <http://wikiba.se/ontology#Item>        -0.1792 -0.1218 0.1969  -0.1986 -0.2870 ...


Wikidata PageRank
https://github.com/athalhammer/danker

- Format: <wikidata entity QID> <page rank score>
- e.g.:
    Q565    74748.496343416002
    Q22664  64425.913967935448
    Q4048908        52860.236505686626


out_file = 'partial_graph.tsv'
graph_embedding_file = '/Volumes/data/repo/data/wikidata/torchbiggraph.tsv'
page_rank_file = '/Volumes/data/repo/data/wikidata/wikidata_pageranks.tsv'



"""
import json
import logging
import os

import fire
from wikimapper import WikiMapper

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


def partial_dump(graph_embedding_file, page_rank_file, out_file, min_page_rank: float = 999.):
    """

    Extract graph embeddings with a page rank above the `min_page_rank` threshold.

    Wikidata graph embeddings from
    - Code: https://github.com/facebookresearch/PyTorch-BigGraph
    - Data (36BG !!!): https://dl.fbaipublicfiles.com/torchbiggraph/wikidata_translation_v1.tsv.gz

    Page rank file from:
    - https://github.com/athalhammer/danker

    :param graph_embedding_file: Path to graph embeddings (from Facebook's PyTorch BigGraph)
    :param page_rank_file:
    :param out_file:
    :param min_page_rank:
    :return:
    """
    selected_entity_ids = set()

    with open(page_rank_file) as fp:
        for i, line in enumerate(fp):
            cols = line.split('\t')

            pg = float(cols[1].strip())

            if pg > min_page_rank:
                selected_entity_ids.add(cols[0])

    print(f'entities with page rank above threshold: {len(selected_entity_ids)}')

    if os.path.exists(out_file):
        os.unlink(out_file)

    with open(out_file, 'a') as out_fp:
        with open(graph_embedding_file) as fp:
            for i, line in enumerate(fp):
                cols = line.split('\t', 1)

                entity_id = cols[0]

                if entity_id.startswith('<http://www.wikidata.org/entity/Q') and entity_id.endswith('>'):
                    entity_id = entity_id.replace('<http://www.wikidata.org/entity/', '').replace('>', '')

                    if entity_id in selected_entity_ids:
                        out_fp.write(entity_id + '\t' + cols[1])

                if not i % 10000:
                    print(f'Lines completed {i}')

    print('Done')


def convert_to_projector(graph_embedding_file, out_file, meta_file, wikimapper_index):
    """

    Convert the graph embedding file to visualize it in Tensorboard Projector,
    and map Wikidata QIDs to Wikipedia titles.


    :param graph_embedding_file:
    :param out_file:
    :param meta_file:
    :param wikimapper_index:
    :return:
    """
    mapper = WikiMapper(wikimapper_index)  # title language is defined in index file

    with open(out_file, 'w') as out_fp:
        with open(meta_file, 'w') as meta_fp:
            with open(graph_embedding_file) as fp:
                for i, line in enumerate(fp):
                    cols = line.split('\t', 1)

                    titles = mapper.id_to_titles(cols[0])

                    if len(titles) < 1:
                        print(f'Could not find any Wikipedia title for {cols[0]}')
                        continue

                    out_fp.write(cols[1])
                    meta_fp.write(titles[0] + '\n')

                    if not i % 1000:
                        print(f'Lines completed {i}')

    print('done')


def merge_topic_json_files(json_directory: str, output_path: str):
    topics = {}
    fns = os.listdir(json_directory)

    for fn in fns:
        if fn.endswith('.json'):
            with open(os.path.join(json_directory, fn)) as f:
                topics.update(json.load(f))

    logger.info(f'Topics merged {len(topics)} from {len(fns)} files')

    with open(output_path, 'w') as f:
        json.dump(topics, f)


if __name__ == '__main__':
    fire.Fire()