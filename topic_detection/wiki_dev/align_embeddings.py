import os

import numpy as np
from gensim.models import KeyedVectors
from wikipedia2vec import Wikipedia2Vec


# https://github.com/Babylonpartners/fastText_multilingual/blob/master/align_your_own.ipynb
# https://torchbiggraph.readthedocs.io/en/latest/pretrained_embeddings.html


def normalized(a, axis=-1, order=2):
    """Utility function to normalize the rows of a numpy array."""
    l2 = np.atleast_1d(np.linalg.norm(a, order, axis))
    l2[l2==0] = 1
    return a / np.expand_dims(l2, axis)


def cos_sim(a, b):
    return np.dot(a, b)/(np.linalg.norm(a)*np.linalg.norm(b))

model_dir = '/Volumes/data/repo/data/wikipedia2vec'


en_wiki2vec = Wikipedia2Vec.load(os.path.join(model_dir, 'dewiki_20180420_100d.pkl'))
de_wiki2vec = Wikipedia2Vec.load(os.path.join(model_dir, 'enwiki_20180420_100d.pkl'))

de_words = set([w.text for w in de_wiki2vec.dictionary.words()])
en_words = set([w.text for w in en_wiki2vec.dictionary.words()])

de_entities = set([e.title for e in de_wiki2vec.dictionary.entities()])
en_entities = set([e.title for e in en_wiki2vec.dictionary.entities()])

overlap_entities = list(de_entities & en_entities)
overlap_words = list(de_words & en_words)

de_list = []
en_list = []

for i, w in enumerate(overlap_words):
    de_list.append(de_wiki2vec.get_word_vector(w))
    en_list.append(en_wiki2vec.get_word_vector(w))

for i, e in enumerate(overlap_entities):
    de_list.append(de_wiki2vec.get_entity_vector(e))
    en_list.append(en_wiki2vec.get_entity_vector(e))

de_matrix = np.array(de_list)
en_matrix = np.array(en_list)

product = np.matmul(normalized(de_matrix).transpose(), normalized(en_matrix))
U, s, V = np.linalg.svd(product)

# return orthogonal transformation which aligns source language to the target
transformation_matrix = np.matmul(U, V)

de_trans = de_wiki2vec.syn0.dot(transformation_matrix)

cos_sim(de_trans[de_wiki2vec.dictionary.get_entity_index('Iran')], en_wiki2vec.get_entity_vector('Iran'))
# 0.6349501
cos_sim(de_wiki2vec.get_entity_vector('Iran'), en_wiki2vec.get_entity_vector('Iran'))
#  -0.109239474

cos_sim(de_trans[de_wiki2vec.dictionary.get_entity_index('Frankreich')], en_wiki2vec.get_entity_vector('France'))
# 0.27232635
cos_sim(de_wiki2vec.get_entity_vector('Frankreich'), en_wiki2vec.get_entity_vector('France'))
#  -0.14754383

###  words only yields better results



de_list = []
en_list = []

for i, e in enumerate(overlap):
    de_list.append(de_wiki2vec.get_entity_vector(e))
    en_list.append(en_wiki2vec.get_entity_vector(e))

de_matrix = np.array(de_list)
en_matrix = np.array(en_list)

product = np.matmul(normalized(de_matrix).transpose(), normalized(en_matrix))
U, s, V = np.linalg.svd(product)

# return orthogonal transformation which aligns source language to the target
transformation_matrix = np.matmul(U, V)

de_trans = de_wiki2vec.syn0.dot(transformation_matrix)



def make_training_matrices(source_dictionary, target_dictionary, bilingual_dictionary):
    """
    Source and target dictionaries are the FastVector objects of
    source/target languages. bilingual_dictionary is a list of
    translation pair tuples [(source_word, target_word), ...].
    """
    source_matrix = []
    target_matrix = []

    for (source, target) in bilingual_dictionary:
        if source in source_dictionary and target in target_dictionary:
            source_matrix.append(source_dictionary[source])
            target_matrix.append(target_dictionary[target])

    # return training matrices
    return np.array(source_matrix), np.array(target_matrix)


de_entities = set([e.title for e in de_wiki2vec.dictionary.entities()])
en_entities = set(en_wiki2vec.dictionary.entities())

overlap = list(en_entities & de_entities)


print(en_wiki2vec.get_entity_vector('Scarlett Johansson'))
print(de_wiki2vec.get_entity_vector('Scarlett Johansson'))

class WikipediaTopics(Wikipedia2Vec):
    pass




def normalized(a, axis=-1, order=2):
    """Utility function to normalize the rows of a numpy array."""
    l2 = np.atleast_1d(np.linalg.norm(a, order, axis))
    l2[l2==0] = 1
    return a / np.expand_dims(l2, axis)


model = KeyedVectors.load_word2vec_format('/Volumes/data/repo/data/glove.6B/glove.6B.200d.w2vformat.txt', binary=False)
model.syn0