import json
import math
import time
from json import JSONDecodeError

import requests

topics = {
    'quora': {
        'property_id': 'P3417',
        'count': 178829,
    }
}

"""

TODO filter Wikipedia2Vec with topic 


title           link                                            count
TED topic ID    https://www.wikidata.org/wiki/Property:P2612    361
Quora topic ID  https://www.wikidata.org/wiki/Property:P3417    178829
JSTOR topic ID  https://www.wikidata.org/wiki/Property:P3827    24532

# count items with property

SELECT (COUNT(?item) AS ?count)
WHERE {
	?item wdt:P3827 ?o .
}


# retrieve items + wikipedia titles with property 

prefix schema: <http://schema.org/>
PREFIX wikibase: <http://wikiba.se/ontology#>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>

SELECT ?cid ?article WHERE {
    ?cid wdt:P3417 ?o .
    OPTIONAL {
      ?article schema:about ?cid .
      ?article schema:inLanguage "en" .
      FILTER (SUBSTR(str(?article), 1, 25) = "https://en.wikipedia.org/")
    }
} 
LIMIT 10

## misch

## Wikidata API example

- One client (user agent + IP) is allowed 60 seconds of processing time each 60 seconds
- One client is allowed 30 error queries per minute


select ?s ?label where {
?s wdt:P3417 ?o .
?s rdfs:label ?label .
FILTER(LANG(?label) = "en")
}
limit 100


select ?s ?p ?o where {
?s wdt:P3417 ?o
}
limit 100

https://query.wikidata.org/bigdata/namespace/wdq/sparql?format=json&query=select ?s ?p ?o where { ?s wdt:P3417 ?o } limit 100

select ?s ?p ?o where { ?s wdt:P3417 ?o } limit 100

"""

# url = f'https://www.wikidata.org/w/api.php?action=query&list=backlinks&bltitle=Property:{topic_property_id}&bllimit={limit}'
# res = requests.get(url)

# """ + topic_property_id + """


def get_sparql_query(property_id: str, limit: int = 10, offset: int = 0, language: str = 'en'):
    query = """
    PREFIX wd: <http://www.wikidata.org/entity/>
    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
    
    SELECT ?qid ?article WHERE {
        ?qid wdt:""" + property_id + """ ?o .
        OPTIONAL {
          ?article schema:about ?qid .
          ?article schema:inLanguage '""" + language + """' .
          FILTER (SUBSTR(str(?article), 1, 25) = 'https://""" + language + """.wikipedia.org/')
        }
    } 
    OFFSET """ + str(offset) + """
    LIMIT """ + str(limit)

    return query

# query = 'select ?s ?p ?o where { ?s wdt:P3417 ?o } limit 100'
# url = 'https://query.wikidata.org/sparql'

api_url = 'https://query.wikidata.org/bigdata/namespace/wdq/sparql'

requests_headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1'
}

"""
84845 items saved
90 	 Offset: 90000


120      Offset: 120000
SPARQL-QUERY: queryStr=
    PREFIX wd: <http://www.wikidata.org/entity/>
    PREFIX wdt: <http://www.wikidata.org/prop/direct/>
    
    SELECT ?qid ?article WHERE {
        ?qid wdt:P3417 ?o .
        OPTIONAL {
          ?article schema:about ?qid .
          ?article schema:inLanguage 'en' .
          FILTER (SUBSTR(str(?article), 1, 25) = 'https://en.wikipedia.org/')
        }
    } 
    OFFSET 120000
    LIMIT 1000
java.util.concurrent.TimeoutException
        at java.util.concurrent.FutureTask.get(FutureTask.java:205)
        at com.bigdata.rdf.sail.webapp.BigdataServlet.submitApiTask(BigdataServlet.java:292)
        at com.bigdata.rdf.sail.webapp.QueryServlet.doSparqlQuery(QueryServlet.java:67
        
"""
# QID => Wikipedia article
qid2wikipedia = {}

topic_name = 'quora'
limit = 1000
topic = topics[topic_name]
start_page = 134  # 120
pages = range(math.ceil(topic['count'] / limit))

for page in pages:  # Call API with pagination
    page_offset = page * limit

    if page < start_page:
        continue

    print(f'{page} \t Offset: {page_offset}')

    r = requests.get(api_url,
                     params={'format': 'json', 'query': get_sparql_query(topic['property_id'], offset=page_offset, limit=limit)},
                     headers=requests_headers)

    try:
        data = r.json()
    except JSONDecodeError:
        print(r.text)
        raise ValueError('Cannot parse JSON')

    for result in data['results']['bindings']:
        qid = result['qid']['value'].replace('http://www.wikidata.org/entity/', '')

        if 'article' in result:
            article = result['article']['value'].replace('https://en.wikipedia.org/wiki/', '')

            qid2wikipedia[qid] = article

    # Save to disk
    with open(f'{topic_name}_{start_page}.json', 'w') as f:
        json.dump(qid2wikipedia, f)

    print(f'{len(qid2wikipedia)} items saved')

    # Wait a little bit
    time.sleep(60 * 5)


## TODO last remaining items p=120      Offset: 120000

