import pickle

import fire
import numpy as np
from smart_open import open
from wikimapper import WikiMapper

"""

python wikidata_for_authors.py run ~/datasets/wikidata/index_enwiki-20190420.db \
    ~/datasets/wikidata/index_dewiki-20190420.db \
    ~/datasets/wikidata/torchbiggraph/wikidata_translation_v1.tsv.gz \
    ~/notebooks/bert-text-classification/authors.pickle \
    ~/notebooks/bert-text-classification/author2embedding.pickle

Found 3684 QIDs for authors (not found: 11779)

"""

# wikimapper_index = '/Volumes/data/repo/data/wikidata/wikimapper/index_enwiki-20190420.db'
# graph_embedding_file = '/Volumes/data/repo/data/wikidata/torchbiggraph.tsv'
# authors_file = '/Volumes/data/repo/text-classification/authors.pickle'
# out_file = 'wikidata_authors.pickle'


def run(wikimapper_index_en, wikimapper_index_de, graph_embedding_file, authors_file, out_file):
    print('Starting...')

    with open(authors_file, 'rb') as f:
        authors_list = pickle.load(f)

    print('Author file loaded')


    en_mapper = WikiMapper(wikimapper_index_en)  # title language is defined in index file
    de_mapper = WikiMapper(wikimapper_index_de)  # title language is defined in index file

    print('WikiMapper loaded (de+en)')

    not_found = 0
    not_found_ = []

    selected_entity_ids = set()
    found = 0

    qid2author = {}

    for book_authors_str in authors_list:
        authors = book_authors_str.split(';')

        for author in authors:

            qid = None

            en_queries = [
                author,
                author.replace(' ', '_'),
                author.replace(' ', '_') + '_(novelist)',
                author.replace(' ', '_') + '_(poet)',
                author.replace(' ', '_') + '_(writer)',
                author.replace(' ', '_') + '_(author)',
                author.replace(' ', '_') + '_(journalist)',
                author.replace(' ', '_') + '_(artist)',
            ]
            for query in en_queries:  # Try all options
                qid = en_mapper.title_to_id(query)
                if qid is not None:
                    break

            # de
            if qid is None:
                de_queries = [
                    author,
                    author.replace(' ', '_'),
                    author.replace(' ', '_') + '_(Dichter)',
                    author.replace(' ', '_') + '_(Schriftsteller)',
                    author.replace(' ', '_') + '_(Autor)',
                    author.replace(' ', '_') + '_(Journalist)',
                    author.replace(' ', '_') + '_(Autorin)',
                ]
                for query in en_queries:  # Try all options
                    qid = de_mapper.title_to_id(query)
                    if qid is not None:
                        break

            if qid is None:
                not_found += 1
                not_found_.append(author)
            else:
                found += 1
                selected_entity_ids.add(qid)
                qid2author[qid] = author

    print(f'Found {len(selected_entity_ids)} QIDs for authors (not found: {not_found})')

    author2embedding = {}


    #with open(graph_embedding_file) as fp:
    with open(graph_embedding_file, encoding='utf-8') as fp:  # smart open can read .gz files
        for i, line in enumerate(fp):
            cols = line.split('\t')

            entity_id = cols[0]

            if entity_id.startswith('<http://www.wikidata.org/entity/Q') and entity_id.endswith('>'):
                entity_id = entity_id.replace('<http://www.wikidata.org/entity/', '').replace('>', '')

                if entity_id in selected_entity_ids:
                    author2embedding[qid2author[entity_id]] = np.array(cols[1:]).astype(np.float)

            if not i % 100000:
                print(f'Lines completed {i}')

    # Save
    with open(out_file, 'wb') as f:
        pickle.dump(author2embedding, f)

    print(f'Saved to {out_file}')
# TODO

# Use full dump + filter for not found names
# https://github.com/maxlath/wikidata-filter

# Scrape https://www.randomhouse.de/Autoren/Uebersicht.rhd

if __name__ == '__main__':
    fire.Fire()