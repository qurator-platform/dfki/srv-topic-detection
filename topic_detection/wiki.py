import json
import logging

import joblib
import numpy as np
from wikimapper import WikiMapper
from wikipedia2vec import Wikipedia2Vec, Dictionary

logger = logging.getLogger(__name__)


class WikiTopics(Wikipedia2Vec):
    """

    wt = WikipediaTopics.load('models/enwiki_20180420_100d.pkl')

    wiki2vec = Wikipedia2Vec.load(MODEL_FILE)
#%%
vs = [wiki2vec.get_word_vector(w) for w, _ in lda_model.show_topic(topic_id, topic_terms) if wiki2vec.get_word(w)]
weights = [weight for w, weight in lda_model.show_topic(topic_id, topic_terms) if wiki2vec.get_word(w)]

#topic_vector = np.mean(vs, 0)
topic_vector = np.average(vs, 0, weights=weights)


wiki2vec.most_similar_by_vector(topic_vector, 10)

entity_syn0 = syn0[entity_offset:]

dst = (np.dot(entity_syn0, topic_vector) / np.linalg.norm(entity_syn0, axis=1) / np.linalg.norm(topic_vector))
indexes = np.argsort(-dst)
sim = [(wd.get_item_by_index(entity_offset + ind), dst[ind]) for ind in indexes[:10]]
#%%
for ent, count in sim:
    if ':' not in ent.title and '#' not in ent.title:
        print(ent.title)

    """
    filtered_entity_syn0 = None
    original_indexes = None
    min_page_rank = None
    entity_idx2qid = {}

    @staticmethod
    def load(in_file, numpy_mmap_mode='c', wikimapper_index_file=None, wikidata_topics_file=None, page_rank_file=None, min_page_rank=None):
        obj = joblib.load(in_file, mmap_mode=numpy_mmap_mode)
        if isinstance(obj['dictionary'], dict):
            dictionary = Dictionary.load(obj['dictionary'])
        else:
            dictionary = obj['dictionary']  # for backward compatibilit

        ret = WikiTopics(dictionary)
        ret.syn0 = obj['syn0']
        ret.syn1 = obj['syn1']
        ret._train_params = obj.get('train_params')

        logging.debug('Wiki2Vec loaded')

        # Load filtered topics
        if wikimapper_index_file is not None and ((min_page_rank is not None and page_rank_file is not None)
                                                  or wikidata_topics_file is not None):
            ret.load_filtered_data(wikimapper_index_file=wikimapper_index_file,
                                   page_rank_file=page_rank_file,
                                   wikidata_topics_file=wikidata_topics_file,
                                   min_page_rank=min_page_rank)
            ret.min_page_rank = min_page_rank

        return ret

    def most_similar_by_entity_vector(self, vec, count=100):
        if self.filtered_entity_syn0 is not None:
            return self.filtered_most_similar_by_entity_vector(vec, count)

        entity_offset = len(self.dictionary._word_stats)
        entity_syn0 = self.syn0[entity_offset:]

        dst = (np.dot(entity_syn0, vec) / np.linalg.norm(entity_syn0, axis=1) / np.linalg.norm(vec))
        indexes = np.argsort(-dst)

        return [(self.dictionary.get_item_by_index(entity_offset + ind), dst[ind]) for ind in indexes[:count]]

    # entity_pageranks

    # title -> qid -> page_rank > min_page_rank

    def load_filtered_data(self, wikimapper_index_file, wikidata_topics_file=None, page_rank_file=None, min_page_rank=500.):
        """

        Filter wiki vectors by
        - wikidata page rank
        - wikidata id

        page_rank_file = '/Volumes/data/repo/data/wikidata/2019-06-07.all.links.rank'
        wikimapper_index_file = '/Volumes/data/repo/data/wikidata/wikimapper/index_enwiki-20190420.db'

        :param wikidata_topics_file: wikidata qid => wikipedia article (english) e.g. quora.json
        :param page_rank_file:
        :param wikimapper_index_file:
        :param min_page_rank:
        :return:
        """

        entity_title2qid = {}

        # Load topic id data
        selected_topic_entity_ids = set()
        if wikidata_topics_file:
            with open(wikidata_topics_file, 'r') as f:
                wikidata_topic_ids = json.load(f)  # wikidata qid => wikipedia article (english)

            for qid in wikidata_topic_ids:
                article_title = wikidata_topic_ids[qid]  # English title (do not use)

                selected_topic_entity_ids.add(qid)

        logger.info(f'Number of entities in Wikidata filter: {len(selected_topic_entity_ids)}')

        # Load page rank data
        selected_page_rank_entity_ids = set()
        if page_rank_file:
            with open(page_rank_file) as fp:
                for i, line in enumerate(fp):
                    cols = line.split('\t')

                    pg = float(cols[1].strip())

                    if pg > min_page_rank:
                        selected_page_rank_entity_ids.add(cols[0])

        logger.info(f'Number of entities with page rank above threshold: {len(selected_page_rank_entity_ids)}')

        # AND Filter for page rank and topic ids
        selected_entity_ids = selected_page_rank_entity_ids & selected_topic_entity_ids

        # Map Wikidata QIDs to Wikipedia titles
        mapper = WikiMapper(wikimapper_index_file)
        selected_entity_titles2qid = {}

        for qid in selected_entity_ids:
            titles = mapper.id_to_titles(qid)

            if titles:
                selected_entity_titles2qid.update({t: qid for t in titles})   # replace _ with ' '

        # Build new embedding index
        # entity_offset = len(self.dictionary._word_stats)
        entities = self.dictionary.entities()
        self.original_indexes = {}
        entity_syn0_list = []

        for entity in entities:
            q = entity.title.replace(' ', '_')
            if q in selected_entity_titles2qid:
                self.original_indexes[len(entity_syn0_list)] = entity.index
                self.entity_idx2qid[entity.index] = selected_entity_titles2qid[q]

                entity_syn0_list.append(self.syn0[entity.index])

        self.filtered_entity_syn0 = np.array(entity_syn0_list)

        logger.info(f'Filtered data loaded. Entities: {len(self.original_indexes)}')

    def filtered_most_similar_by_entity_vector(self, vec, count=100):
        if self.filtered_entity_syn0 is None or self.original_indexes is None:
            raise ValueError('Page rank data is not loaded. You must call `load_filtered_data()` in advance!')

        dst = (np.dot(self.filtered_entity_syn0, vec) / np.linalg.norm(self.filtered_entity_syn0, axis=1) / np.linalg.norm(vec))
        indexes = np.argsort(-dst)

        for ind in indexes[:count]:
            original_ind = self.original_indexes[ind]
            yield (
                self.dictionary.get_item_by_index(original_ind),  # entity
                dst[ind],  # similarity score
                self.entity_idx2qid[original_ind],  # Wikidata QID
            )

