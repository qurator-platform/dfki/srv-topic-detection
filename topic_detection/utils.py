import logging
import re
from typing import List

import nltk
from nltk import WordNetLemmatizer

from topic_detection.language import Language

logger = logging.getLogger(__name__)


def tokenize(language: Language, text, cut_long_text=False, max_text_length=1000000):
     # Spacy NLP has a maximum text lengh
    if len(text) < max_text_length:
        for token in language.nlp(text):

            if token.orth_.isspace():
                pass
            elif token.like_url:
                yield 'URL'
            elif token.orth_.startswith('@'):
                yield 'SCREEN_NAME'
            else:
                yield token.lower_

    else:
        if cut_long_text:
            return tokenize(language, text[:max_text_length])

        logger.warning('Text too long for Spacy NLP. Using split tokenzier instead!')
        return [t for t in text.split()]


def prepare_text_for_lda(language: Language, text, min_token_length=4, max_text_length=1000000, cut_long_text=False) -> List[str]:
    tokens = tokenize(language, text, cut_long_text=cut_long_text, max_text_length=max_text_length)
    tokens = [token for token in tokens if len(token) > min_token_length]
    tokens = [token for token in tokens if token not in language.stop_words and re.search(r'^([a-zA-Z]{2,})$', token)]
    tokens = [get_lemma(token) for token in tokens]
    return tokens


def get_lemma(word):
    # TODO should depend on language
    lemma = nltk.corpus.wordnet.morphy(word)
    if lemma is None:
        return word
    else:
        return lemma


def get_lemma2(word):
    return WordNetLemmatizer().lemmatize(word)