import re
from xml.etree import cElementTree

from gensim.corpora.wikicorpus import get_namespace, filter_wiki
from gensim.scripts.segment_wiki import extract_page_xmls


def get_pages_from_wiki_dump(wiki_dump_path, max_doc_count=0):
    """

    :param wiki_dump_path: Path to /enwikinews-XXX-pages-meta-current.xml
    :param max_doc_count: Limits the number of documents returned
    :return:
    """
    category_pattern = re.compile('\[\[(Category|Kategorie):(.*?)\]\]')

    with open(wiki_dump_path, 'rb') as xml_fileobj:
        page_xmls = extract_page_xmls(xml_fileobj)
        i = 0

        docs = []

        for i, page_xml in enumerate(page_xmls):
            # s = segment(page_xml, include_interlinks=True)
            # print(page_xml)

            elem = cElementTree.fromstring(page_xml)
            filter_namespaces = ('0',)
            namespace = get_namespace(elem.tag)
            ns_mapping = {"ns": namespace}
            text_path = "./{%(ns)s}revision/{%(ns)s}text" % ns_mapping
            title_path = "./{%(ns)s}title" % ns_mapping
            ns_path = "./{%(ns)s}ns" % ns_mapping

            title = elem.find(title_path).text
            text = elem.find(text_path).text
            ns = elem.find(ns_path).text
            if ns not in filter_namespaces:
                continue

            categories = [c for _, c in category_pattern.findall(text)]

            cleaned_text = filter_wiki(text)

            if '#REDIRECT' in cleaned_text or '#redirect' in cleaned_text:
                continue

            docs.append({
                'title': title,
                'text': cleaned_text,
                'categories': categories,
            })

            if 0 < max_doc_count < len(docs):
                break

    return docs
