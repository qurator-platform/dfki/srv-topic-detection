__version__ = '0.1.0'


class Topic(object):
    """

    corpus
    lda_terms
    wikidata_items
    wikipedia_articles

    topic_terms = self.lda_model.show_topic(topic_id, self.terms_per_topic)

    term_vectors = [wiki_topics.get_word_vector(w) for w, _ in topic_terms if wiki_topics.get_word(w)]
    term_weights = [weight for w, weight in topic_terms if wiki_topics.get_word(w)]

    topic_vector = np.average(term_vectors, 0, weights=term_weights)


    """

    def __init__(self, topic_id, lda_terms, lda_term_vectors, lda_term_weights, topic_vector, wiki_candidates):
        self.topic_id = topic_id
        self.lda_terms = lda_terms
        self.lda_term_vectors = lda_term_vectors
        self.lda_term_weights = lda_term_weights
        self.topic_vector = topic_vector
        self.wiki_candidates = wiki_candidates

    def to_dict(self):
        return {
            'id': self.topic_id,
            'terms': [t[0] for t in self.lda_terms],
            'weights': [float(t[1]) for t in self.lda_terms],
            #'lda_term_vectors': self.lda_term_vectors,
            #'lda_term_weights': self.lda_term_weights,
            #'topic_vector': self.topic_vector,
            'wiki_candidates': [{
                'wikipedia_title': w[0].title,
                'score': float(w[1]),
                'wikidata_qid': w[2] if len(w) > 2 else None,
            } for w in self.wiki_candidates]
        }
