import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

MODEL_DIR = os.getenv('MODEL_DIR', os.path.join(BASE_DIR, 'models'))

SPACY_MODEL = os.getenv('SPACY_MODEL', 'en_core_web_sm')
NLTK_STOP_WORDS = os.getenv('NLTK_STOP_WORDS', 'english')

SPACY_MODEL_DE = os.getenv('SPACY_MODEL_DE', 'de_core_news_sm')
NLTK_STOP_WORDS_DE = os.getenv('NLTK_STOP_WORDS_DE', 'german')

