import json
import logging.config
import os
from unittest import TestCase

import numpy as np
from sklearn.datasets import fetch_20newsgroups

from topic_detection.corpus import Corpus, LANGUAGES
from topic_detection.lda import get_topics
from topic_detection.utils import prepare_text_for_lda
from topic_detection.wiki import WikiTopics

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

log_file_path = os.path.join(BASE_DIR, 'topic_detection', 'tests', 'test_logging.conf')
logging.config.fileConfig(fname=log_file_path, disable_existing_loggers=False)

logger = logging.getLogger(__name__)


class TopicDetectionTest(TestCase):


        # for t in c.get_topic_labels_from_lda().items():
        #     print(t)

    # def test_wikipedia_topics(self):
    #     wiki =  WikipediaTopics.load(os.path.join(BASE_DIR, 'models', 'enwiki_20180420_100d.pkl'))
    #
    #     print(type(wiki))
    #     # print(BASE_DIR)

    def test_wikipedia_topics(self):
        wt =  WikiTopics.load(os.path.join(BASE_DIR, 'models', 'enwiki_20180420_100d.pkl'))


        kws = ["vote", "election", "people", "usa", "clinton", "trump"]

        vs = [wt.get_word_vector(w) for w in kws]
        tv = np.mean(vs, 0)

        print(wt.most_similar_by_vector(tv, 10))
        print('---')
        print(wt.most_similar_by_entity_vector(tv, 10))
        print('---')
        ##

        kws = ["iphone", "ipod", "itunes", "jobs"]

        vs = [wt.get_word_vector(w) for w in kws]
        tv = np.mean(vs, 0)

        print(wt.most_similar_by_vector(tv, 10))
        print('---')
        print(wt.most_similar_by_entity_vector(tv, 10))


    def test_doc2topic(self):
        doc = fetch_20newsgroups(subset="test", shuffle=False).data[0]

        c = Corpus('test', LANGUAGES['en'], 5)
        c.load()

        print(get_topics(doc, c, 2))

    def test_get_topics(self):
        doc = fetch_20newsgroups(subset="test", shuffle=False).data[0]

        lang = LANGUAGES['en']

        c = Corpus('test', language=lang, num_topics=5)
        c.load()

        prepared_doc = prepare_text_for_lda(language=c.language, text=doc)

        doc_bow = c.dictionary.doc2bow(prepared_doc)

        topics = [(c.topics[topic_id][0][0].title, prop) for topic_id, prop in c.lda_model.get_document_topics(doc_bow)]

        print(prepared_doc)

        print(f'Topics: {topics}')


    def test_get_lda_topics(self):
        text = '''Wie Trump mitteilte, fühle sich Ratcliffe in den Medien „sehr unfair“ behandelt. Auf Twitter schrieb der Präsident, Ratcliffe wolle lieber „im Kongress bleiben“, statt sich solchen „Schmähungen“ auszusetzen. Trump erklärte, er habe einige andere Kandidaten in petto. Später sagte Trump, er würde dem Staat Geld ersparen, indem er Kandidaten benenne, ohne sie zuvor durchleuchten zu lassen. Er würde einfach einen Namen nennen, und die Presse würde die nötigen Nachforschungen betreiben. '''
        c = Corpus.load_from_disk('enwikinews', os.path.join(BASE_DIR, 'models', 'enwikinews'), 'en', num_topics=10)
        topics = get_topics(text, c)

        # print(topics)

        json.dumps(topics)
#
