import logging.config
import os
from unittest import TestCase

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

log_file_path = os.path.join(BASE_DIR, 'topic_detection', 'tests', 'test_logging.conf')
logging.config.fileConfig(fname=log_file_path, disable_existing_loggers=False)

logger = logging.getLogger(__name__)


class TopicDetectionTest(TestCase):
    def test_logging(self):
        logger.error('foo')

        logger.debug('bar')

