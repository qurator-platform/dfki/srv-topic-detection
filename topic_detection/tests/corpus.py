import logging.config
import os
from unittest import TestCase

from sklearn.datasets import fetch_20newsgroups

from topic_detection.corpus import Corpus, LANGUAGES
from topic_detection.wiki import WikiTopics

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

log_file_path = os.path.join(BASE_DIR, 'topic_detection', 'tests', 'test_logging.conf')
logging.config.fileConfig(fname=log_file_path, disable_existing_loggers=False)

logger = logging.getLogger(__name__)


class CorpusTest(TestCase):
    def test_train_and_save_corpus(self):
        test_docs = fetch_20newsgroups(subset="train", shuffle=False).data
        wt = WikiTopics.load(os.path.join(BASE_DIR, 'models', 'enwiki_20180420_100d.pkl'))

        c = Corpus('test', LANGUAGES['en'], 5)
        c.train(test_docs, wt)
        c.save()

    def test_train_and_save_corpus2(self):
        test_docs = fetch_20newsgroups(subset="test", shuffle=False).data
        wt = WikiTopics.load(os.path.join(BASE_DIR, 'models', 'enwiki_20180420_100d.pkl'))

        c = Corpus('test2',  LANGUAGES['en'], 10)
        c.train(test_docs, wt)
        c.save()

        pass

    def test_load_corpus(self):
        c = Corpus('test',  LANGUAGES['en'], 5)
        c.load()

        print(c.topics)

    def test_load_corpus_from_disk(self):
        c = Corpus.load_from_disk('enwikinews', os.path.join(BASE_DIR, 'models', 'enwikinews'), 'en', num_topics=10)


        print(c.topics)
