from typing import List

from topic_detection.corpus import Corpus
from topic_detection.utils import prepare_text_for_lda


def get_topics(doc: str, corpus: Corpus, num_topics: int = 3) -> List:
    """

    Process input document and perform LDA

    :param num_topics: Number of of topic to be returned
    :param doc: Document as plain text string
    :param corpus: Corpus object defining language etc
    :return: List
    """

    prepared_doc = prepare_text_for_lda(corpus.language, doc)

    doc_bow = corpus.dictionary.doc2bow(prepared_doc)
    topics = []

    for topic_id, score in corpus.lda_model.get_document_topics(doc_bow)[:num_topics]:

        topics.append({
            'topic': corpus.topics[topic_id].to_dict(),
            'score': float(score),
        })

    return topics
