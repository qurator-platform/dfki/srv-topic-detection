import logging.config
import os

from flask import Flask, Blueprint
from flask_restplus import Resource, apidoc
from werkzeug.exceptions import BadRequest

from qurator_app import ReverseProxied, CustomAPI
from qurator_app.settings import SERVICE_PATH, FLASK_PATH, FLASK_DEBUG, FLASK_HOST, FLASK_PORT
from topic_detection.corpus import Corpus
from topic_detection.lda import get_topics

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

log_file_path = os.path.join(BASE_DIR, 'flask_logging.conf')

logging.config.fileConfig(fname=log_file_path, disable_existing_loggers=False)

logger = logging.getLogger(__name__)

# Manually defined corpora
corpora = {
    # Dummy
    'test': Corpus.load_from_disk('test', 'example_models/test', 'en', 5),
    'test2': Corpus.load_from_disk('test2', 'example_models/test2', 'en', 5),

    # Real corpora
    # 'bbcsport': Corpus.load_from_disk('bbcsport', 'example_models/bbcsport', 'en', 5),
    'bbc': Corpus.load_from_disk('bbc', 'example_models/bbc', 'en', 5),
    'bbc30': Corpus.load_from_disk('bbc30', 'example_models/bbc30', 'en', 30),

    'enwikinews': Corpus.load_from_disk('enwikinews', 'example_models/enwikinews', 'en', 10),
    'dewikinews': Corpus.load_from_disk('dewikinews', 'example_models/dewikinews', 'de', 30),

}

logger.info(f'Available corpora: {corpora.keys()}')

logger.info(f'FLASH_HOST: {FLASK_HOST}')
logger.info(f'FLASK_PORT: {FLASK_PORT}')
logger.info(f'FLASK_PATH: {FLASK_PATH}')
logger.info(f'FLASK_DEBUG: {FLASK_DEBUG}')
logger.info(f'SERVICE_PATH: {SERVICE_PATH}')

app = Flask(__name__)
app.config['DEBUG'] = FLASK_DEBUG
app.wsgi_app = ReverseProxied(app.wsgi_app, script_name=SERVICE_PATH)

blueprint = Blueprint('api', __name__, url_prefix=FLASK_PATH)

api = CustomAPI(blueprint,
                version='0.1',
                title='Topic Detection API',
                description='Detects topics in arbitrary document texts. Topic labels are Wikipedia pages (or WikiData items).',
                default='Topic detection',
                default_label='',
                )
app.register_blueprint(blueprint)
app.config['DEBUG'] = FLASK_DEBUG

# api.namespaces = []


post_parser = api.parser()
post_parser.add_argument('corpus', type=str, required=True, help='Corpus name')
post_parser.add_argument('doc', type=str, required=True, help='Unprocessed document text')
post_parser.add_argument('num_topics', type=int, required=False, default=1, help='Number of topics to be returned')


@api.route('/topics/')
class TopicResource(Resource):
    # def get(self):
    #     """
    #     List available corpora.
    #
    #     The returned corpora are available for topic detection.
    #
    #     """
    #     return list(corpora.keys())

    @api.expect(post_parser)
    def post(self):
        """
        Detect topics in unseen documents.

        """
        args = post_parser.parse_args()  # works with POST,GET,JSON

        if args['corpus'] in corpora:
            corpus = corpora[args['corpus']]
        else:
            raise BadRequest('Requested corpus does not exist.')

        topics = get_topics(doc=args['doc'], corpus=corpus, num_topics=args['num_topics'])

        return {
            'topics': topics,
            'input': args,
        }


@api.route('/corpora/')
class CorporaResource(Resource):
    def get(self):
        """
        List available corpora.

        The returned corpora are available for topic detection.

        """
        return list(corpora.keys())


@api.route('/corpus/<string:corpus_id>')
class CorpusResource(Resource):
    def get(self, corpus_id):
        """
        View corpus details.
        """

        if corpus_id in corpora:
            corpus = corpora[corpus_id]
        else:
            raise BadRequest('Requested corpus does not exist.')

        return corpus.to_dict()

    # @api.expect(post_parser)
    # def post(self):
    #     """
    #     Detect topics in unseen documents.
    #
    #     """
    #     raise BadRequest('Training currently not possible.')


@api.documentation
def swagger_ui():
    return apidoc.ui_for(api)


if __name__ == '__main__':
    # Use main only for debugging. Production should use gunicorn.
    app.run(
        debug=FLASK_DEBUG,
        host=FLASK_HOST,
        port=FLASK_PORT,
    )