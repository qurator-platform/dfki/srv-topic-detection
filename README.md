# Topic detection

**STATUS: PROOVE-OF-CONCEPT**

## Requirements

- Wikipedia2Vec: larger models require up to 16 GB memory (see [Pretrained Embeddings](https://wikipedia2vec.github.io/wikipedia2vec/pretrained/))
- Wikimapper: [Pre-computed models](https://github.com/jcklie/wikimapper#precomputed-indices)

- [Reverse proxy settings for deployment as service.](https://blog.macuyiko.com/post/2016/fixing-flask-url_for-when-behind-mod_proxy.html)

## Examples

```bash
$ curl -XPOST localhost:5000/ -d '{"corpus":"a", "doc":"b"}' -H "Content-Type: application/json"
```

## Open issues

- Irrelevant Wikipedia pages as topic labels
    - Filter by popularity
    - Discard non-article pages (File:, Category:, Lists ...)
- LDA only works for equally distributed topics
- Corpus management
    - Train topic models for new corpora
   
   
## Installation

### Manual (Debugging)

```bash
pip install -r requirements.txt
./download_models.sh
python flask_app.py
```

 
### Docker

```bash
# Build with docker
docker build -t td . 

# Run (log and model dirs are mounted)
docker run -p 80:8000 \
    -v $(pwd)/models:/app/models \
    -v $(pwd)/logs:/app/logs td
```

### Use as Python module

Install as Python module:

```bash
git clone https://gitlab.com/qurator-platform/dfki/srv-topic-detection.git
cd srv-topic-detection
pip install .
./download_models.sh
```

Use:

```python
from topic_detection.wikipedia import WikipediaTopics
from topic_detection.corpus import Corpus 
from topic_detection.lda import get_topics

# Load Wikipedia embeddings (run download_models.sh before)
wt = WikipediaTopics.load('models/enwiki_20180420_100d.pkl')

# New corpus named `foo` in english language and 10 topics
c = Corpus('foo', 'en', num_topics=10)

# Each doc is represented as str
docs = ['foo bar. bar foo!', 'this is a test. foo.']

# Build corpus + LDA model + find matching wiki pages
c.train(docs, wikipedia_topics=wt)

# Trained topics
print(f'Available topics: {c.topics}')

# Assign topics to a new document
get_topics('hello foo test.', c, num_topics=2)
>> [(0, 'Portal:Baseball', 0.5193260312080383), (2, 'Chief of the Armed Forces (Malaysia)', 0.14923140406608582)]
```

## Train a new corpus

```bash
Usage: train.py [OPTIONS] CORPUS_NAME LANGUAGE DOCS_DIR [WIKIPEDIA2VEC_FILE]

Options:
  --num-topics INTEGER  Number of topics

Example:

python train.py dummy_corpus en data/docs/dummy/ models/enwiki_20180420_100d.pkl
```


### BBC corpus

```bash
wikipedia2vec_model_file = models/enwiki_20180420_100d.pkl

page_rank_file data/wikidata/2019-06-07.all.links.rank
wikimapper_index_file data/wikidata/wikimapper/index_enwiki-20190420.db
min_page_rank=1000

```


## Settings

You can change the following settings with environment variables.

| Variable name | Default value | Comment |
| ------------- | ------------- | ------- |
| `FLASK_HOST` | `localhost` | |
| `FLASK_PORT` | `5000` | |
| `FLASK_PATH` | `` | |
| `FLASK_DEBUG` | `-1` | |


## Debug with Nginx proxy

```bash
docker run --name srv-proxy \
 -v $(pwd)/nginx/html/:/usr/share/nginx/html/:ro \
 -v $(pwd)/nginx/conf/dummy.conf:/etc/nginx/sites-enabled/dummy.conf:ro \
 -p 8080:80 \ 
 nginx
 
 
docker run --rm \
 -v $(pwd)/nginx/html/:/usr/share/nginx/html/ \
 -v $(pwd)/nginx/conf/:/etc/nginx/sites-enabled/:ro \
 -p 8080:80 nginx
```


## License

Internal use only