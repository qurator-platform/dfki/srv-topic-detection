FROM python:3.6
LABEL maintainer="malte.ostendorff@dfki.de"

ENV FLASK_DEBUG=-1
ENV FLASK_PATH=""
ENV FLASK_HOST="0.0.0.0"
ENV FLASK_PORT=8000

WORKDIR /app

ADD ./requirements.txt /app/requirements.txt
ADD . /app

RUN pip install -r requirements.txt

# download models
RUN mkdir -p /app/logs

RUN mkdir -p /app/models
RUN ./download_models.sh

RUN python -m nltk.downloader stopwords
RUN python -m nltk.downloader wordnet

EXPOSE 8000

# CMD ["gunicorn", "-b", "0.0.0.0:8000", "flask_app:app"]
#CMD ["sbin/app.sh"]
ENTRYPOINT ["sbin/app.sh"]