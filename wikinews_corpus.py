import gensim
from gensim import corpora

from topic_detection.corpus import LANGUAGES
from topic_detection.utils import prepare_text_for_lda

wiki_dump_path = '/Volumes/data/repo/data/wikinews/enwikinews-20190120-pages-meta-current.xml'

lang = LANGUAGES['en']

num_topics = 5
terms_per_topic = 10

cleaned_docs = [prepare_text_for_lda(lang, doc['text']) for doc in docs]

dictionary = corpora.Dictionary(cleaned_docs)
corpus = [dictionary.doc2bow(doc) for doc in cleaned_docs]

lda_model = gensim.models.LdaMulticore(
            corpus,
            num_topics=num_topics,
            id2word=dictionary,
            workers=2
        )

for topic_id in range(lda_model.num_topics):
    topic_terms = lda_model.show_topic(topic_id, terms_per_topic)
    print(f'#{topic_id}: {topic_terms}\n')

