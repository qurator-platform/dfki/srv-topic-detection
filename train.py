import os
import click

import logging
import logging.config

from topic_detection.corpus import Corpus
from topic_detection.wiki import WikipediaTopics

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

log_file_path = os.path.join(BASE_DIR, 'flask_logging.conf')

logging.config.fileConfig(fname=log_file_path, disable_existing_loggers=False)

logger = logging.getLogger(__name__)


@click.command()
@click.argument('corpus_name')
@click.argument('language')
@click.argument('docs_dir', type=click.Path(exists=True))
@click.argument('wikipedia2vec_file', default='models/enwiki_20180420_100d.pkl', type=click.Path(exists=True))
@click.option('--num-topics', default=10, help='Number of topics')
def train(corpus_name, language, docs_dir, wikipedia2vec_file, num_topics):
    """
    CLI for training

    :param corpus_name: Name of the corpus to train
    :param language: Language of document text
    :param wikipedia2vec_file:
    :param docs_dir: Directory containing document text files
    :param num_topics:
    :return:
    """

    # Load Wikipedia embeddings
    wt = WikipediaTopics.load(wikipedia2vec_file)

    c = Corpus(corpus_name, language, num_topics)

    # read docs from txt files
    docs = []
    for file_name in os.listdir(docs_dir):
        with open(os.path.join(docs_dir, file_name), 'r') as f:
            docs.append(f.read())

    c.train(docs, wiki_topics=wt)

    logger.info('done')


if __name__ == '__main__':
    train()