import os

SERVICE_PATH = os.getenv('ELG_HTTP_PATH_PREFIX', '/srv/td')
FLASK_PATH = os.getenv('FLASK_PATH', '')
FLASK_DEBUG = os.getenv('FLASK_DEBUG', False)
FLASK_HOST = os.getenv('FLASK_HOST', 'localhost')
FLASK_PORT = os.getenv('FLASK_PORT', 5000)