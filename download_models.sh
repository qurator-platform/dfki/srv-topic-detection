#!/bin/bash

mkdir -p models

# Wikipedia2Vec embeddings

#wget http://wikipedia2vec.s3.amazonaws.com/models/en/2018-04-20/enwiki_20180420_100d.pkl.bz2
#bzip2 -d enwiki_20180420_100d.pkl.bz2
#mv enwiki_20180420_100d.pkl.bz2 models

# NLTK
python -m nltk.downloader stopwords

# Spacy
pip install https://github.com/explosion/spacy-models/releases/download/en_core_web_sm-2.1.0/en_core_web_sm-2.1.0.tar.gz#egg=en_core_web_sm