#!/usr/bin/env python

from __future__ import print_function

import codecs
import os
import re

from setuptools import setup, find_packages


def read(*parts):
    filename = os.path.join(os.path.dirname(__file__), *parts)
    with codecs.open(filename, encoding='utf-8') as fp:
        return fp.read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


setup(
    name='topic-detection',
    version=find_version('topic_detection', '__init__.py'),
    url='https://qurator.ai/',
    license='private',
    description='Topic Detection',
    long_description=read('README.md'),
    long_description_content_type='text/markdown',
    author='Malte Ostendorff',
    author_email='malte.ostendorff@dfki.de',
    packages=['topic_detection'],
    install_requires=[
        'flask-restplus',
        'wikipedia2vec',
        'gensim',
        'spacy>=2.1.0',
        'nltk',

        'gunicorn',

        # Packages from dependency links
        # ...
    ],
    dependency_links=[
    ],
    include_package_data=True,
    classifiers=[
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Topic :: Utilities',
    ],
    zip_safe=False,
)
